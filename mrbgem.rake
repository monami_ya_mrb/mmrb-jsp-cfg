MRuby::Gem::Specification.new('mmrb-jsp-cfg') do |spec|
  spec.license = 'TOPPERS License'
  spec.authors = 'www.toppers.jp'
  spec.summary = 'Kernel configurator for TOPPERS/JSP'
  spec.bins = %w(cfg chk)

  spec.cxx.include_paths << %W(#{dir}/src/cfg)
end
